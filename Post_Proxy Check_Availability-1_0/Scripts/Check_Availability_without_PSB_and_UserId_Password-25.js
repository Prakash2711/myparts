setVariablesIfNeeded('{ds}/Resources/Microservices.csv','JsDPL',0,'');
addGlobalHeader("Content-Type","application/x-www-form-urlencoded");

var $regUrl = $URL+"embedded/1.0/proxy"
var $partsBasket = 
{
  "version": "1.2",
  "uid": "12345",
  "action": "checkAvailabilityRequest",
  "vehicleInfo": {
    "ymmeNo": "",
    "ymme": "",
    "vin": "",
    "licencePlate": "",
    "aaia": "",
    "aces": ""
  },
  "lineInfo": {
    "line": [
      {
        "lineNo": "1",
        "type": "part",
        "manufacturerCode": "WIX",
        "partNumber": "12121",
        "brand": " ",
        "description": "",
        "quantityRequested": "2",
        "unitOfMeasure": "EA",
        "unitCorePrice": "3",
        "unitCostPrice": "12",
        "priceOverride": "false",
        "priceOverrideMsg": " "
      }
    ]
  }
};
var payload = new formData();
payload.put("partsBasket",JSON.stringify($partsBasket));

var $responsereg = post($regUrl,payload);

//log($regUrl,$partsBasket);
assertEquals($responsereg.status,400);
setVariablesIfNeeded('{ds}/Resources/Microservices.csv','JsDPL',0,'');
addGlobalHeader("Content-Type","application/x-www-form-urlencoded");

var $regUrl = $URL+"embedded/1.0/proxy"
var $partsBasket = 
{
  "version": "1.1",
  "action": "checkAvailabilityRequest",
  "vehicleInfo": {
    "ymmeNo": "",
    "ymme": "",
    "licencePlate": ""
  },
  "lineInfo": {
    "line": [
      {
        "brand": "",
        "partCategory": "",
        "description": "",
        "manufacturerCode": "PMP",
        "partNumber": "51515",
        "priceOverride": false,
        "priceOverrideMsg": "",
        "unitCorePrice": 1,
        "unitCostPrice": 10,
        "unitListPrice": 12,
        "unitListCore": 1,
        "quantityAvailable": 1,
        "quantityRequested": 4,
        "locationId": "100",
        "locationDescription": "",
        "status": "fnd",
        "minOrderQty": 1,
        "unitOfMeasure": "EA",
        "lineNo": 1,
        "seqNo": 1,
        "type": "part",
        "skillLevel": "",
        "warrantyHrs": 0,
        "hrlyCostRate": 0,
        "hrlyListRate": 0,
        "incrementalOrderQty": 1,
        "locationStatus": "primary",
        "laborCode": "",
        "quantityShipped": 0,
        "mitchellHrs": 0,
        "value": "",
        "gfx": true
      }
    ]
  }
};
var payload = new formData();

payload.put("partsBasket",JSON.stringify($partsBasket));
payload.put("userId","aa999jsmith");
payload.put("password","123456");
payload.put("partnerId","AB Magique");
payload.put("sellerId","aa999ABM ");
payload.put("buyerId","JoesStore");
payload.put("partsBasket",JSON.stringify($partsBasket));

var $responsereg = post($regUrl,payload);

//log($regUrl,$partsBasket);
assertEquals($responsereg.status,200);
setVariablesIfNeeded('{ds}/Resources/Microservices.csv','JsDPL',0,'');
addGlobalHeader("Content-Type","application/x-www-form-urlencoded");

var $regUrl = $URL+"embedded/1.0/proxy"
var $partsBasket = 
{
  "version": "1.2",
  "uid": "12345",
  "action": "checkAvailabilityRequest",
  "vehicleInfo": {
    "ymmeNo": "",
    "ymme": "",
    "vin": "",
    "licencePlate": "",
    "aaia": "",
    "aces": ""
  },
  "lineInfo": {
    "line": [
      {
        "lineNo": "1",
        "type": "part",
        "manufacturerCode": "WIX",
        "partNumber": "51515",
        "brand": " ",
        "description": "",
        "quantityRequested": "1",
        "unitOfMeasure": "EA",
        "unitCorePrice": "3",
        "unitCostPrice": "12",
        "priceOverride": "false",
        "priceOverrideMsg": " "
      }
    ]
  }
};
var payload = new formData();
payload.put("userId","aa999jsmith");
payload.put("password","123456");
payload.put("partnerId","AB Magique");
payload.put("sellerId","aa999ABM ");
payload.put("buyerId","JoesStore");
payload.put("partsBasket",JSON.stringify($partsBasket));

var $responsereg = post($regUrl,payload);

//log($regUrl,$partsBasket);
assertEquals($responsereg.status,200);
include("{ds}/Token_API_Include.js");
addGlobalHeader("Authorization",$idToken);
addGlobalHeader("Content-Type","application/json");
addGlobalHeader("Catalog-Index","proident Lorem nisi dolore");
addGlobalHeader("Service-Name","MyPlace");
addGlobalHeader("Partner-Name","Alliance Development");
var $regUrl1 = "https://qa.ms.myplace4parts.com/part/1.0/part/byNavigation?catalogId=111&countryId=us"
var $CreateRequest1 = 
    {
    "start": 0,
    "limit": 50,
    "clientUTCOffset": -6.5,
    "filters": [],
    "sortOrder": [],
    "cgts": [
        {
            "category": {
                "id": 3,
                "value": "Exhaust & Clutch "
            },
            "group": {
                "id": 38,
                "value": "Exhaust Parts"
            },
            "terminology": {
                "id": 99186,
                "value": "Muffler, Pipes & Related Parts"
            }
        },
        {
            "category": {
                "id": 3,
                "value": "Exhaust & Clutch "
            },
            "group": {
                "id": 38,
                "value": "Exhaust Parts"
            },
            "terminology": {
                "id": 401,
                "value": "Axle Pipe "
            }
        }
    ],
    "vehicle": {
        "year": {
            "id": 2010,
            "value": "2010"
        },
        "make": {
            "id": 8,
            "value": "FORD"
        },
        "modelType": {
            "id": 8,
            "value": "CAR"
        },
        "model": {
            "id": 19,
            "value": "FUSION"
        },
        "engine": {
            "id": 17,
            "value": "4-2488 2.5L DOHC(VIN 3, A)"
        }
    }
};
var $responsereg1 = post($regUrl1,$CreateRequest1);
log($responsereg1);
var $jsonObj1 = JSON.parse($responsereg1.getDataString());
log($jsonObj1);
log($regUrl1,$CreateRequest1);
//locker.put("idToken",$jsonObj.idToken);
var $idToken = $jsonObj1.idToken;
log($idToken);
var $partsBinID = $jsonObj1.partsBinId
log($partsBinID);
assertEquals($responsereg1.status,200);
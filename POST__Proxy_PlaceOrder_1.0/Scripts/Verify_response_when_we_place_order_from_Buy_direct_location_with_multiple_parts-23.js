setVariablesIfNeeded('{ds}/Resources/Microservices.csv','JsDPL',0,'');
addGlobalHeader("Content-Length","<calculated when request is sent>");
addGlobalHeader("Host","<calculated when request is sent>");
addGlobalHeader("User-Agent","PostmanRuntime/7.29.2");
addGlobalHeader("Accept","*/*");
addGlobalHeader("Accept-Encoding","gzip, deflate, br");
addGlobalHeader("Connection","keep-alive");
//addGlobalHeader("Content-Type","application/x-www-form-urlencoded");

var $regUrl = $URL+"embedded/1.0/proxy"
var $partsBasket = 
{
  "version": "1.2",
  "uid": "1234",
  "action": "placeOrderRequest",
  "vehicleInfo": {
    "ymmeNo": "",
    "ymme": "1990",
    "vin": "",
    "aaia": "",
    "licencePlate": "",
    "aces": ""
  },
  "orderInfo": {
    "deliveryMethod": "REGULAR",
    "orderMessage": "test Order",
    "poNumber": "test Order"
  },
  "deliverToInfo": {
    "attention": "",
    "name": "",
    "address": {
      "line1": "HMT hills",
      "line2": "",
      "city": "",
      "region": "",
      "country": "",
      "postalCode": ""
    },
    "email": "",
    "phoneNum": ""
  },
  "lineInfo": {
    "line": [
     
	  	  {
        "lineNo": "2",
        "type": "part",
        "manufacturerCode": "PMP",
        "partNumber": "51515",
        "brand": " ",
        "description": "Oil filter",
        "quantityRequested": 1,
        "unitOfMeasure": "EA",
        "unitCorePrice": "9999.99",
        "unitCostPrice": "9999.99",
        "priceOverride": "false",
        "priceOverrideMsg": " ",
        "locationId": "102",
        "locationStatus": "buyDirect",
        "locationDescription": "Store 2"
      },
	  {
        "lineNo": "3",
        "type": "part",
        "manufacturerCode": "GAT",
        "partNumber": "12121",
        "brand": " ",
        "description": "Oil filter",
        "quantityRequested": 1,
        "unitOfMeasure": "EA",
        "unitCorePrice": "9999.99",
        "unitCostPrice": "9999.99",
        "priceOverride": "false",
        "priceOverrideMsg": " ",
        "locationId": "102",
        "locationStatus": "alternate",
        "locationDescription": "Store 3"
      }
	  
    ]
  }
};
var payload = new formData();
payload.put("userId","aa999jsmith");
payload.put("password","123456");
payload.put("partsBasket",JSON.stringify($partsBasket));

var $responsereg = post($regUrl,payload);

assertEquals($responsereg.status,200);
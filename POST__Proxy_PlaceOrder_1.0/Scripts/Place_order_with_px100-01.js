setVariablesIfNeeded('{ds}/Resources/Microservices.csv','JsDPL',0,'');
addGlobalHeader("Content-Length","<calculated when request is sent>");
addGlobalHeader("Host","<calculated when request is sent>");
addGlobalHeader("User-Agent","PostmanRuntime/7.29.2");
addGlobalHeader("Accept","*/*");
addGlobalHeader("Accept-Encoding","gzip, deflate, br");
addGlobalHeader("Connection","keep-alive");
//addGlobalHeader("Content-Type","application/x-www-form-urlencoded");

var $regUrl = $URL+"embedded/1.0/proxy"
var $partsBasket = 
{
  "version": "1.1",
  "uid": "1234",
  "action": "placeOrderRequest",
  "lineInfo": {
    "line": [
      {
        "brand": "",
        "description": "Copper plus sm eng",
        "manufacturerCode": "WIX",
        "partNumber": "51515",
        "priceOverride": false,
        "priceOverrideMsg": "",
        "unitCorePrice": 10.00,
        "unitCostPrice": 10.00,
        "quantityRequested": 20,
        "unitOfMeasure": "EA",
        "lineNo": 1,
        "type": "part",
        "locationId": "100",
        "locationStatus": "primary",
        "locationDescription": "Store 1"
      }
    ]
  },
  "orderInfo": {
    "deliveryMethod": "URGENT",
    "orderMessage": "Test Order",
    "p0Number": "Test order"
  },
  "vehicleInfo": {
    "ymmeNo": "",
    "ymme": "",
    "vin": "",
    "licencePlate": "",
    "aaia": "",
    "aces": ""
  }
};
var payload = new formData();
//payload.put("userId","aa999jsmith");
//payload.put("password","123456");
payload.put("partnerId","Mitchell");
payload.put("sellerId","ac002Mit");
payload.put("buyerId","AMPPTestGarage");
payload.put("partsBasket",JSON.stringify($partsBasket));

var $responsereg = post($regUrl,payload);

assertEquals($responsereg.status,200);
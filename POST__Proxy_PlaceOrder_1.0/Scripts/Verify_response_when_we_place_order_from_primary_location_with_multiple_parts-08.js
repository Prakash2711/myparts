setVariablesIfNeeded('{ds}/Resources/Microservices.csv','JsDPL',0,'');
addGlobalHeader("Content-Length","<calculated when request is sent>");
addGlobalHeader("Host","<calculated when request is sent>");
addGlobalHeader("User-Agent","PostmanRuntime/7.29.2");
addGlobalHeader("Accept","*/*");
addGlobalHeader("Accept-Encoding","gzip, deflate, br");
addGlobalHeader("Connection","keep-alive");
//addGlobalHeader("Content-Type","application/x-www-form-urlencoded");

var $regUrl = $URL+"embedded/1.0/proxy"
var $partsBasket = 
{
  "version": "1.1",
  "uid": "1234",
  "action": "placeOrderRequest",
 "vehicleInfo": {
    "ymmeNo": "",
    "ymme": "1990",
    "vin": "",
    "aaia": "",
    "licencePlate": "",
    "aces": ""
  },
  
  "lineInfo": {
    "line": [
      {
        "brand": "",
        "description": "Copper plus sm eng",
        "manufacturerCode": "WIX",
        "partNumber": "BR5568",
        "priceOverride": false,
        "priceOverrideMsg": "",
        "unitCorePrice": 3,
        "unitCostPrice": 12,
        "quantityRequested": 1,
        "unitOfMeasure": "EA",
        "lineNo": 1,
        "type": "part",
        "locationId": "100",
        "locationStatus": "buyDirect",
        "locationDescription": "Store 5"
      },
	  {
        "brand": "",
        "description": "Copper plus sm eng",
        "manufacturerCode": "WIX",
        "partNumber": "BR5558",
        "priceOverride": false,
        "priceOverrideMsg": "",
        "unitCorePrice": 3,
        "unitCostPrice": 12,
        "quantityRequested": 1,
        "unitOfMeasure": "EA",
        "lineNo": 10,
        "type": "part",
        "locationId": "100",
        "locationStatus": "alternate",
        "locationDescription": "Store 7"
      }
    ]
  },
  "orderInfo": {
    "deliveryMethod": "REGULAR",
    "orderMessage": "Test Order",
    "pONumber": "TestPO"
  }
};
var payload = new formData();
//payload.put("userId","aa999jsmith");
//payload.put("password","123456");
payload.put("partnerId","Mitchell");
payload.put("sellerId","ac002Mit");
payload.put("buyerId","AMPPTestGarage");
payload.put("partsBasket",JSON.stringify($partsBasket));

var $responsereg = post($regUrl,payload);

assertEquals($responsereg.status,200);
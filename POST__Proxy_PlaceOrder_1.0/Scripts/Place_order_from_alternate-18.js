setVariablesIfNeeded('{ds}/Resources/Microservices.csv','JsDPL',0,'');
addGlobalHeader("Content-Length","<calculated when request is sent>");
addGlobalHeader("Host","<calculated when request is sent>");
addGlobalHeader("User-Agent","PostmanRuntime/7.29.2");
addGlobalHeader("Accept","*/*");
addGlobalHeader("Accept-Encoding","gzip, deflate, br");
addGlobalHeader("Connection","keep-alive");
//addGlobalHeader("Content-Type","application/x-www-form-urlencoded");

var $regUrl = $URL+"embedded/1.0/proxy"
var $partsBasket = 
{
  "version": "1.2",
  "uid": "54678",
  "action": "placeOrderRequest",
  "lineInfo": {
    "line": [
      {
        "brand": " ",
        "description": "Copper plus sm eng",
        "manufacturerCode": "GAT",
        "partNumber": "350200",
        "priceOverride": true,
        "priceOverrideMsg": " ",
        "unitCorePrice": 1.0,
        "unitCostPrice": 9999.99,
        "quantityRequested": 30,
        "unitOfMeasure": "EA",
        "lineNo": "1",
        "type": "part",
        "locationId": "102",
        "locationStatus": "alternate",
        "locationDescription": "Store 3"
      }
    ]
  },
  "orderInfo": {
    "deliveryMethod": "REGULAR",
    "orderMessage": "test Order",
    "poNumber": "test Order"
  },
  "vehicleInfo": {
    "ymmeNo": "",
    "ymme": "Car",
    "vin": "eww345trf",
    "licencePlate": "218 ymc:PA",
    "aaia": "1234",
    "aces": "5467"
  },
  "deliverToInfo": {
    "attention": "123",
    "name": "haritha",
    "address": {
      "line1": "123",
      "line2": "456",
      "city": "789",
      "region": "098",
      "country": "12312",
      "postalCode": "321"
    },
    "email": "haritha.c@gmail.com",
    "phoneNum": "0987654321345678"
  }
};
var payload = new formData();
payload.put("userId","aa999jsmith");
payload.put("password","123456");
payload.put("partnerId","Mitchell");
payload.put("sellerId","ac002Mit");
payload.put("buyerId","AMPPTestGarage");
payload.put("partsBasket",JSON.stringify($partsBasket));

var $responsereg = post($regUrl,payload);

assertEquals($responsereg.status,200);
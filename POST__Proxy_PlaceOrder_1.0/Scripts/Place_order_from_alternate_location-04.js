setVariablesIfNeeded('{ds}/Resources/Microservices.csv','JsDPL',0,'');
addGlobalHeader("Content-Length","<calculated when request is sent>");
addGlobalHeader("Host","<calculated when request is sent>");
addGlobalHeader("User-Agent","PostmanRuntime/7.29.2");
addGlobalHeader("Accept","*/*");
addGlobalHeader("Accept-Encoding","gzip, deflate, br");
addGlobalHeader("Connection","keep-alive");
//addGlobalHeader("Content-Type","application/x-www-form-urlencoded");

var $regUrl = $URL+"embedded/1.0/proxy"
var $partsBasket = 
{
  "version": "1.1",
  "uid": "1234",
  "action": "placeOrderRequest",
  "vehicleInfo": {
    "ymmeNo": "",
    "ymme": "2010 HONDA CIVIC 4-1998 2.0L DOHC",
    "vin": "5Y2SL648X3Z478820",
    "aaia": "",
    "licencePlate": "PA JGS8742",
    "aces": ""
},
  
  "lineInfo": {
    "line": [
      {
        "brand": "",
        "description": "Copper plus sm eng",
        "manufacturerCode": "WIX",
        "partNumber": "PH8A",
        "priceOverride": false,
        "priceOverrideMsg": "",
        "unitCorePrice": 1.0,
        "unitCostPrice": 9999.99,
        "quantityRequested": 30,
        "unitOfMeasure": "EA",
        "lineNo": 1,
        "type": "part",
        "locationId": "101",
        "locationStatus": "alternate",
        "locationDescription": "Store 2"
      }
    ]
  },
  "orderInfo": {
    "deliveryMethod": "REGULAR",
    "orderMessage": "Test Order",
    "pONumber": "TestPO"
  }
};
var payload = new formData();
//payload.put("userId","aa999jsmith");
//payload.put("password","123456");
payload.put("partnerId","Mitchell");
payload.put("sellerId","ac002Mit");
payload.put("buyerId","AMPPTestGarage");
payload.put("partsBasket",JSON.stringify($partsBasket));

var $responsereg = post($regUrl,payload);

assertEquals($responsereg.status,200);
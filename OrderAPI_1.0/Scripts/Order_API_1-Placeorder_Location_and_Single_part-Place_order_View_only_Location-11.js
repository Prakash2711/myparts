setVariablesIfNeeded('{ds}/Microservices.csv','JsDPL',0,'');
include("{ds}/Token_API_Include.js");
addGlobalHeader("Authorization",$idToken);
addGlobalHeader("Service-Name","MyPlace");
addGlobalHeader("Catalog-Index","0");
addGlobalHeader("Partner-Name","Alliance Development");
var $regUrl1 = $URL+"order/1.0/order?catalogId=111";
var $CreateRequest1 = 
{
  "vehicle": {
    "year": "",
    "make": "",
    "model": "",
    "engine": ""
  },
  "deliveryDetail": {
    "deliveryMethod": "Urgent",
    "shipCode": "",
    "carrier": "",
    "releaseCode": "",
    "attentionToUser": "",
    "name": "",
    "address": {
      "line1": "",
      "line2": "",
      "city": "",
      "region": "",
      "country": "",
      "postalCode": ""
    },
    "email": "",
    "phoneNum": ""
  },
  "noteToStore": "Test ORder,DON'T PICKUP",
  "poNumber": "1234567890-0987654321",
  "comment": "A",
  "locatePartMsg": true,
  "parts": [
      {
            "lineNo": 2,
            "lineCode": "SRA",
            "manufacturerName": "Schrader Automotive",
            "partNumber": "33500-150",
            "brand": "Motorcraft",
            "description": "Tire Pressure Monitoring System Sensor",
            "quantityRequested": 1,
            "unitOfMeasure": "EA",
            "unitCorePrice": 100,
            "unitCostPrice": 100,
            "isPriceOverride": false,
            "priceOverrideMsg": "",
            "locationId": "105",
            "locationDescription": "12345678"
        }
  ],
  "isTestOrder": true
};


var $responsereg1 = post($regUrl1,$CreateRequest1);
//var $jsonObj1 = JSON.parse($responsereg1.getDataString());
log($regUrl1,$CreateRequest1);
assertEquals($responsereg1.status,200);
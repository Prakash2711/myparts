setVariablesIfNeeded('{ds}/Microservices.csv','JsDPL',0,'');
include("{ds}/Token_API_Include.js");
addGlobalHeader("Authorization",$idToken);
addGlobalHeader("Content-Type","application/json");
addGlobalHeader("Service-Name","MyPlace");
addGlobalHeader("Catalog-Index","0");
addGlobalHeader("Partner-Name","Alliance Development");
var $regUrl1 = $URL+"order/1.0/order?catalogId=";
var $CreateRequest1 = 
{
   "vehicle": {
    "year": "",
    "make": "",
    "model": "",
    "engine": ""
  },
  "deliveryDetail": {
    "deliveryMethod": "Test Order",
    "shipCode": "place_carrier",
    "releaseCode": "Will Call",
    "attentionToUser": "",
    "name": "aa999 sam's garage",
    "address": {
      "line1": "US",
      "line2": "Dearborn",
      "city": "New YORK",
      "region": "HA",
      "country": "US",
      "postalCode": "TES789654123"
    },
    "email": "test@gmail.com",
    "phoneNum": "7878476546545"
  },
  "noteToStore": "Test ORder,DON'T PICKUP",
  "poNumber": "Test ORder",
  "comment": "A",
  "locatePartMsg": false,
  "parts": [
      {
      "lineNo": 1,
      "lineCode":"GAT",
      "manufacturerName": "GATE",
      "partNumber": "KM5248",
      "brand": "Motorcraft",
      "description": "Upper Radiator Or Coolant Hose",
      "quantityRequested": 1,
      "unitOfMeasure": "EA",
      "unitCorePrice":100.99,
      "unitCostPrice": 9999.99,
      "isPriceOverride": false,
      "priceOverrideMsg": "",
      "locationId": "100",
      "locationDescription": "asdfgh"
    },
    {
      "lineNo": 2,
      "lineCode":"FRA",
      "manufacturerName": "WIX",
      "partNumber": "KS2IA",
      "brand": "Motorcraft",
      "description": "Upper Radiator Or Coolant Hose",
      "quantityRequested": 1,
      "unitOfMeasure": "EA",
      "unitCorePrice":205.99,
      "unitCostPrice": 10.99,
      "isPriceOverride": false,
      "priceOverrideMsg": "",
      "locationId": "105",
      "locationDescription": "asdfgh"
    }
  ],
  "isTestOrder": true
};
var $responsereg1 = post($regUrl1,$CreateRequest1);
//var $jsonObj1 = JSON.parse($responsereg1.getDataString());
log($regUrl1,$CreateRequest1);
assertEquals($responsereg1.status,400);
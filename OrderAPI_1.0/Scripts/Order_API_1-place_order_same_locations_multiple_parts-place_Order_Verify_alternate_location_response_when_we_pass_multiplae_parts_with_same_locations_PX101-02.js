setVariablesIfNeeded('{ds}/Microservices.csv','JsDPL',0,'');
include("{ds}/Token_API_Include.js");
addGlobalHeader("Authorization",$idToken);
addGlobalHeader("Content-Type","application/json");
addGlobalHeader("Service-Name","MyPlace");
addGlobalHeader("Catalog-Index","0");
addGlobalHeader("Partner-Name","Alliance Development");
var $regUrl1 = $URL+"order/1.0/order?catalogId=111";
var $CreateRequest1 = 
{
    "vehicle": {
        "year": "2020",
        "make": "DODGE",
        "model": "DURANGO",
        "engine": "V6-3604 3.6L DOHC(VIN G)"
    },
    "deliveryDetail": {
        "deliveryMethod": "Regular",
        "shipCode": "place_carrier",
        "carrier": "txt_carrier",
        "releaseCode": "Will Call",
        "attentionToUser": "",
        "name": "aa999 sam's garage",
        "address": {
            "line1": "US",
            "line2": "Dearborn",
            "city": "New YORK",
            "region": "HA",
            "country": "US",
            "postalCode": "TES789654123"
        },
        "email": "test@gmail.com",
        "phoneNum": "7878476546545"
    },
    "noteToStore": "Test ORder,DON'T PICKUP",
    "poNumber": "Test ORder",
    "comment": "A",
    "parts": [
        {
            "lineNo": 1,
            "lineCode": "SRA",
            "manufacturerName": "Schrader Automotive",
            "partNumber": "33500-150",
            "brand": "Motorcraft",
            "description": "Tire Pressure Monitoring System Sensor",
            "quantityRequested": 99,
            "unitOfMeasure": "EA",
            "unitCorePrice": 0,
            "unitCostPrice": 10,
            "isPriceOverride": false,
            "priceOverrideMsg": "",
            "locationId": "101",
            "locationDescription": "Test1"
        },
        {
            "lineNo": 2,
            "lineCode": "SRA",
            "manufacturerName": "Schrader Automotive",
            "partNumber": "33500-150",
            "brand": "Motorcraft",
            "description": "Tire Pressure Monitoring System Sensor",
            "quantityRequested": 1,
            "unitOfMeasure": "EA",
            "unitCorePrice": 0,
            "unitCostPrice": 10,
            "isPriceOverride": false,
            "priceOverrideMsg": "",
            "locationId": "101",
            "locationDescription": "Test"
        }
    ],
    "isTestOrder": true
};
var $responsereg1 = post($regUrl1,$CreateRequest1);
//var $jsonObj1 = JSON.parse($responsereg1.getDataString());
log($regUrl1,$CreateRequest1);
assertEquals($responsereg1.status,200);
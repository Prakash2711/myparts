setVariablesIfNeeded('{ds}/Microservices.csv','JsDPL',0,'');
include("{ds}/Token_API_Include.js");
addGlobalHeader("Authorization",$idToken);
addGlobalHeader("Content-Type","application/json");
addGlobalHeader("Service-Name","MyPlace");
addGlobalHeader("Catalog-Index","0");
addGlobalHeader("Partner-Name","Alliance Development");
var $regUrl1 = $URL+"order/1.0/order?catalogId=111";
var $CreateRequest1 = 
{
  "deliveryDetail": {
    "deliveryMethod": "Regular",
    "shipCode": "REGULAR",
    "carrier": "txt_carrier",
    "releaseCode": "TEST ORDER",
    "attentionToUser": "",
    "name": "aa999 sam's garage",
    "address": {
      "line1": "US",
      "line2": "",
      "city": "nEW YORK",
      "region": "HA",
      "country": "US",
      "postalCode": "TES789654123"
    },
    "email": "test@gmail.com",
    "phoneNum": "7878476546545"
  },
  "isTestOrder": true,
  "noteToStore": "Test ORder,DON'T PICKUP",
  "parts": [
    {
	  "isPriceOverride": false,
      "lineNo": 1,
      "lineCode":"HOC",
	  "manufacturerName": "Oil Pan (Engine)",
	  "partNumber": "HOP17B",
	  "quantityRequested": 1,
	  "unitCorePrice": 5,
      "unitCostPrice": 10,
	  "brand": "ADS",
	  "description": "Oil Pan (Engine)",
	  "unitOfMeasure": "EA",
      "priceOverrideMsg": "",
      "locationId": "105",
      "locationDescription": "LOC 1"
     },
     {
	  "isPriceOverride": false,
      "lineNo": 2,
      "lineCode":"HOC",
	  "manufacturerName": "Oil Pan (Engine)",
	  "partNumber": "HOP17B",
	  "quantityRequested": 1,
	  "unitCorePrice": 9999,
      "unitCostPrice": 9999,
	  "brand": "ADS",
	  "description": "Oil Pan (Engine)",
	  "unitOfMeasure": "EA",
      "priceOverrideMsg": "",
      "locationId": "105",
	  "locationDescription": "LOC1"
     }
     ],
    "vehicle": {
    "year": "2010",
    "make": "HONDA",
    "model": "CIVIC",
    "engine": "4-1998 2.0L DOHC"
  },
  "poNumber": "Test ORder",
  "comment": "A",
  "locatePartMsg": false
  
};


var $responsereg1 = post($regUrl1,$CreateRequest1);
//var $jsonObj1 = JSON.parse($responsereg1.getDataString());
log($regUrl1,$CreateRequest1);
assertEquals($responsereg1.status,200);
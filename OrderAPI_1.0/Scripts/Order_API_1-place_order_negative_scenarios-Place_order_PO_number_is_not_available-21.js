setVariablesIfNeeded('{ds}/Microservices.csv','JsDPL',0,'');
include("{ds}/Token_API_Include.js");
addGlobalHeader("Authorization",$idToken);
addGlobalHeader("Service-Name","MyPlace");
addGlobalHeader("Catalog-Index","0");
addGlobalHeader("Partner-Name","Alliance Development");
var $regUrl1 = $URL+"order/1.0/order?catalogId=111";
var $CreateRequest1 = 
{
  "vehicle": {
     "year": "2021",
    "make": "AUDI",
    "model": "A4",
    "engine": "4-1984 2.0L DOHC"
  },
  "deliveryDetail": {
    "deliveryMethod": "R",
    "shipCode": "",
    "carrier": "",
    "releaseCode": "",
    "attentionToUser": "",
    "name": "",
    "address": {
      "line1": "123 Lucky Lane",
      "line2": null,
      "city": "Lac Du Bonnet",
      "region": "QC",
      "country": "CA",
      "postalCode": "R0E 1A0"
    },
    "email": "haritha.c@gmail.com",
    "phoneNum": ""
  },
  "noteToStore": "",
  "poNumber": "123456",
  "comment": "",
  "locatePartMsg": true,
  "parts": [
      {
      "lineNo": 1,
      "lineCode":"",
      "manufacturerName": "GATE",
      "partNumber": "KM5248",
      "brand": "Motorcraft",
      "description": "Upper Radiator Or Coolant Hose",
      "quantityRequested": 1,
      "unitOfMeasure": "EA",
      "unitCorePrice":100.0,
      "unitCostPrice": 100.0,
      "isPriceOverride": true,
      "priceOverrideMsg": "true",
      "locationId": "100",
      "locationDescription": "Test order"
    }
    ],
  "isTestOrder": true
}


var $responsereg1 = post($regUrl1,$CreateRequest1);
//var $jsonObj1 = JSON.parse($responsereg1.getDataString());
log($regUrl1,$CreateRequest1);
assertEquals($responsereg1.status,400);
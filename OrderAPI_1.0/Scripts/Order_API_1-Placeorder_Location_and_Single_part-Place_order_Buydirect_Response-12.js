setVariablesIfNeeded('{ds}/Microservices.csv','JsDPL',0,'');
include("{ds}/Token_API_Include.js");
addGlobalHeader("Authorization",$idToken);
addGlobalHeader("Service-Name","MyPlace");
addGlobalHeader("Catalog-Index","0");
addGlobalHeader("Partner-Name","Alliance Development");
var $regUrl1 = $URL+"order/1.0/order?catalogId=111";
var $CreateRequest1 = 
{
  "vehicle": {
    "year": "2010",
    "make": "FORD",
    "model": "FOCUS",
    "engine": "4-121 2.0L DOHC(VIN N)"
  },
  "deliveryDetail": {
    "deliveryMethod": "Regular",
    "carrier": "cr desc",
    "shipCode": "REGULAR",
    "releaseCode": "Test related details",
    "attentionToUser": "",
    "name": "aa999 sam's garage",
    "address": {
      "line1": "US",
      "line2": "",
      "city": "nEW YORK",
      "region": "HA",
      "country": "US",
      "postalCode": "TES789654123"
    },
    "email": "test@gmail.com",
    "phoneNum": "7878476546545"
  },
  "noteToStore": "Test ORder,DON'T PICKUP",
  "poNumber": "Test ORder",
  "comment": "A",
  "parts": [
      {
      "lineNo": 1,
      "lineCode":"GAT",
      "manufacturerName": "GATE",
      "partNumber": "KM5248",
      "brand": "Motorcraft",
      "description": "Upper Radiator Or Coolant Hose",
      "quantityRequested": 1,
      "unitOfMeasure": "EA",
      "unitCorePrice":9999.99,
      "unitCostPrice": 9999.99,
      "isPriceOverride": false,
      "priceOverrideMsg": "",
      "locationId": "101",
      "locationDescription": "12345tygfdsa"
    }
    
    ],
  "isTestOrder": true
};
var $responsereg1 = post($regUrl1,$CreateRequest1);
//var $jsonObj1 = JSON.parse($responsereg1.getDataString());
log($regUrl1,$CreateRequest1);
assertEquals($responsereg1.status,200);
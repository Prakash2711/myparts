setVariablesIfNeeded('{ds}/Microservices.csv','JsDPL',0,'');
include("{ds}/Token_API_Include.js");
addGlobalHeader("Authorization",$idToken);
addGlobalHeader("Service-Name","MyPlace");
addGlobalHeader("Catalog-Index","0");
addGlobalHeader("Partner-Name","Alliance Development");
var $regUrl1 = $URL+"order/1.0/order?catalogId=111";
var $CreateRequest1 = 
{
    "vehicle": {
        "year": "",
        "make": "",
        "model": "",
        "engine": ""
    },
    "deliveryDetail": {
        "deliveryMethod": "Regular",
        "shipCode": "",
        "releaseCode": "",
        "attentionToUser": "",
        "name": "",
        "address": {
            "line1": "HMT hills",
            "line2": "",
            "city": "",
            "region": "",
            "country": "",
            "postalCode": ""
        },
        "email": "",
        "phoneNum": ""
    },
    "noteToStore": null,
    "poNumber": "9809765",
    "comment": "test Order",
    "locatePartMsg": false,
    "parts": [
        {
            "lineNo": 1,
            "lineCode": "PQS",
            "status": null,
            "manufacturerName": "PQS",
            "partNumber": "BR5568",
            "brand": " ",
            "description": "Copper plus sm eng",
            "quantityRequested": 1,
            "quantitiyShipped": 0,
            "unitOfMeasure": "EA",
            "unitCorePrice": 9999.99,
            "unitCostPrice": 9999.99,
            "unitListCore": null,
            "unitListPrice": null,
            "isPriceOverride": false,
            "priceOverrideMsg": " ",
            "locationId": 100,
            "locationDescription": "Store 114"
        }
    ],
    "isTestOrder": true
};

var $responsereg1 = post($regUrl1,$CreateRequest1);
//var $jsonObj1 = JSON.parse($responsereg1.getDataString());
log($regUrl1,$CreateRequest1);
assertEquals($responsereg1.status,200);
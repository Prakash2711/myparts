setVariablesIfNeeded('{ds}/Microservices.csv','JsDPL',0,'');
include("{ds}/Token_API_Include.js");
addGlobalHeader("Authorization",$idToken);
addGlobalHeader("Content-Type","application/json");
addGlobalHeader("Service-Name","MyPlace");
addGlobalHeader("Catalog-Index","0");
addGlobalHeader("Partner-Name","Alliance Development");
var $regUrl1 = $URL+"order/1.0/order?catalogId=111";
var $CreateRequest1 = 
{
  "vehicle": {
    "year": "2010",
    "make": "BMW",
    "model": "M6",
    "engine": "10-4999 5.0L DOHC"
  },
  "deliveryDetail": {
    "deliveryMethod": "Regular",
    "carrier": "txt_carrier",
    "shipCode": "REGULAR",
    "releaseCode": "TEST ORDER",
    "attentionToUser": "",
    "name": "aa999 sam's garage",
    "address": {
      "line1": "US",
      "line2": "",
      "city": "nEW YORK",
      "region": "HA",
      "country": "US",
      "postalCode": "TES789654123"
    },
    "email": "test@gmail.com",
    "phoneNum": "7878476546545"
  },
  "noteToStore": "Test ORder,DON'T PICKUP",
  "poNumber": "Test ORder",
  "comment": "A",
  "parts": [
      {
      "lineNo": 1,
      "lineCode":"STA",
      "manufacturerName": "Rear Wheel Stud",
      "partNumber": "TPM2012VK25P",
      "brand": "GAT",
      "description": "Dorman - Autograde",
      "quantityRequested": 4,
      "unitOfMeasure": "EA",
      "unitCorePrice": 100,
      "unitCostPrice": 50,
      "isPriceOverride": false,
      "priceOverrideMsg": "",
      "locationId": "102",
      "locationDescription": "LOC 2"
    },
    {
      "lineNo": 2,
      "lineCode":"GAT",
      "manufacturerName": "Rear Wheel Stud",
      "partNumber": "TPM2012VK25P",
      "brand": "Dorman - Autograde",
      "description": "Dorman - Autograde",
      "quantityRequested": 4,
      "unitOfMeasure": "EA",
      "unitCorePrice": 100,
      "unitCostPrice": 50,
      "isPriceOverride": false,
      "priceOverrideMsg": "",
      "locationId": "102",
      "locationDescription": "LOC 2"
    }
    
  ],
  "isTestOrder": true
};

var $responsereg1 = post($regUrl1,$CreateRequest1);
//var $jsonObj1 = JSON.parse($responsereg1.getDataString());
log($regUrl1,$CreateRequest1);
assertEquals($responsereg1.status,200);
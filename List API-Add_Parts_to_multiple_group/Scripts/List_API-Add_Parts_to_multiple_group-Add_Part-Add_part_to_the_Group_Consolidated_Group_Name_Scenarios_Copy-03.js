setVariablesIfNeeded('{ds}/Resources/Microservices.csv','JsDPL',0,'');
include("{ds}/Token_API_Include.js");
include("{ds}/List_API-Add_Parts_to_multiple_group-Add_List-Create_List-02.js");
var lId=$listId.toString();
log(lId);
addGlobalHeader("Authorization",$idToken);
addGlobalHeader("ListId",lId);
addGlobalHeader("Content-Type","application/json");
addGlobalHeader("Accept","application/json");
addGlobalHeader("Content-Length","<calculated when request is sent>");
addGlobalHeader("Host","<calculated when request is sent>");
addGlobalHeader("User-Agent","PostmanRuntime/7.29.2");
addGlobalHeader("Accept","*/*");
addGlobalHeader("Accept-Encoding","gzip, deflate, br");
addGlobalHeader("Connection","keep-alive");
var $regUrl1 = $URL+"list/1.0/list/"+lId+"/part";
var $CreateRequest1 = 
[
    {
        "seqNo": 1,
        "groupName": "Test",
        "lineCode": "GAT",
        "partNumber": "30GROUP",
        "stockQty": 1,
        "comment": "Part 1"
    },
    {
        "seqNo": 2,
        "groupName": "",
        "lineCode": "GAT",
        "partNumber": "12345",
        "stockQty": 2,
        "comment": "Part 2"
    },
    {
        "seqNo": 3,
        "groupName": "Test",
        "lineCode": "GAT",
        "partNumber": "12121",
        "stockQty": 1,
        "comment": "Part 3"
    },
    {
        "seqNo": 4,
        "groupName": "Base",
        "lineCode": "GAT",
        "partNumber": "51515",
        "stockQty": 1,
        "comment": "Part 4"
    }
];
var $responsereg1 = post($regUrl1,$CreateRequest1);
//var $jsonObj1 = JSON.parse($responsereg1.getDataString());
log($regUrl1,$CreateRequest1);
assertEquals($responsereg1.status,200);
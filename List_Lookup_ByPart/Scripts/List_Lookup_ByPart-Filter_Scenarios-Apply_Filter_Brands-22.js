setVariablesIfNeeded('{ds}/Resources/Microservices.csv','JsDPL',0,'');
include("{ds}/Token_API_Include.js");
addGlobalHeader("Authorization",$idToken);
addGlobalHeader("Content-Type","application/json");
addGlobalHeader("Accept","application/json");
addGlobalHeader("Content-Length","<calculated when request is sent>");
addGlobalHeader("Host","<calculated when request is sent>");
addGlobalHeader("User-Agent","PostmanRuntime/7.29.2");
addGlobalHeader("Accept","*/*");
addGlobalHeader("Accept-Encoding","gzip, deflate, br");
addGlobalHeader("Connection","keep-alive");
addGlobalHeader("Service-Name","MyPlace");
addGlobalHeader("Catalog-Index","1");
addGlobalHeader("Partner-Name","Alliance Development");
addGlobalHeader("Accept","application/json");
addGlobalHeader("countryId","us");

var $regUrl1 = $URL+"/part/byParts?catalogId=99&countryId=us";
var $CreateRequest1 = 
{
    "start": 0,
    "limit": 15,
    "clientUTCOffset": 5.5,
    "filters": 
    [
        {
            "groupName": "Part Type",
            "items": [
                "Suction line hose assembly"
            ]
        },
        {
            "groupName": "Brands",
            "items": [
                "Four Seasons"
            ]
        }
    ],
    "sortOrder": [],
    "parts": [
        {
            "partNumber": "56008",
            "lineCode": "FSE",
            "description": "ABC",
            "qty": 2
        },
        {
            "partNumber": "350200",
            "lineCode": "GAT",
            "description": "GATES, GATES PRODUCT",
            "qty": 2
        },
        {
            "partNumber": "9305HD",
            "lineCode": "GAT",
            "description": "ALTERNATOR BELT",
            "qty": 1
        },
        {
            "partNumber": "51515",
            "lineCode": "WIX",
            "description": "OIL FILTER",
            "qty": 1
        },
        {
            "partNumber": "041-8159",
            "lineCode": "B04",
            "description": "OIL FILTER",
            "qty": 1
        },
        {
            "partNumber": "785177",
            "lineCode": "CWK",
            "description": "SUCTION LINE",
            "qty": 1
        },
        {
            "partNumber": "17305",
            "lineCode": "DAY",
            "description": "ALTERNATOR BELT",
            "qty": 1
        },
        {
            "partNumber": "61515",
            "lineCode": "PM",
            "description": "PARTS MASTE PRODUCT",
            "qty": 5
        },
        {
            "partNumber": "39105",
            "lineCode": "EDH",
            "description": "EDELMANN PRODUCT",
            "qty": 5
        },
        {
            "partNumber": "7516",
            "lineCode": "GAT",
            "description": "A/P & FAN BELT",
            "qty": 4
        },
        {
            "partNumber": "12121 ",
            "lineCode": "MPM",
            "description": "SUCTION LINE",
            "qty": 2
        },
        {
            "partNumber": "56008",
            "lineCode": "FSE",
            "description": "FACTORY AIR PRODUCT",
            "qty": 7
        }
    ]
};
var $responsereg1 = post($regUrl1,$CreateRequest1);
//var $jsonObj1 = JSON.parse($responsereg1.getDataString());
log($regUrl1,$CreateRequest1);
assertEquals($responsereg1.status,200);
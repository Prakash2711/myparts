setVariablesIfNeeded('{ds}/Resources/Microservices.csv','JsDPL',0,'');
include("{ds}/Token_API_Include.js");
addGlobalHeader("Authorization",$idToken);
addGlobalHeader("Content-Type","application/json");
addGlobalHeader("Accept","application/json");
addGlobalHeader("Content-Length","<calculated when request is sent>");
addGlobalHeader("Host","<calculated when request is sent>");
addGlobalHeader("User-Agent","PostmanRuntime/7.29.2");
addGlobalHeader("Accept","*/*");
addGlobalHeader("Accept-Encoding","gzip, deflate, br");
addGlobalHeader("Connection","keep-alive");
addGlobalHeader("Service-Name","MyPlace");
addGlobalHeader("Catalog-Index","1");
addGlobalHeader("Partner-Name","Alliance Development");
addGlobalHeader("Accept","application/json");
addGlobalHeader("countryId","us");

var $regUrl1 = $URL+"/part/byParts?catalogId=99&countryId=us";
var $CreateRequest1 = 
{
    "start": 0,
    "limit": 20,
    "clientUTCOffset": 5.5,
    "parts": [
        {
            "partNumber": "K080865",
            "lineCode": "GAT",
            "description": "",
            "qty": 1
        }
    ]
};
var $responsereg1 = post($regUrl1,$CreateRequest1);
//var $jsonObj1 = JSON.parse($responsereg1.getDataString());
log($regUrl1,$CreateRequest1);
assertEquals($responsereg1.status,200);
setVariablesIfNeeded('{ds}/Resources/Microservices.csv','JsDPL',0,'');
include("{ds}/Token_API_Include.js");
addGlobalHeader("Authorization",$idToken);

addGlobalHeader("Content-Type","application/json");
addGlobalHeader("Accept","application/json");
addGlobalHeader("Host","<calculated when request is sent>");
addGlobalHeader("User-Agent","PostmanRuntime/7.29.2");
addGlobalHeader("Accept","*/*");
addGlobalHeader("Accept-Encoding","gzip, deflate, br");
addGlobalHeader("Connection","keep-alive");
var promoId="999";
var $regUrl1 = $URL+"/promotion/"+promoId+"/entries";
var $responsereg1 = get($regUrl1);
assertEquals($responsereg1.status,404);